### MySQL-Review

#### 介绍
结合高性能MySQL第三版和面试以及实践经验而来的MySQL深度复盘和解读。

#### 软件架构
- 是自我学习的总结，经验与教训。
- 针对MySQL高性能以及其原理的探究。
- 如果您发现有错误的地方，可以添加 Issue 。

#### 安装教程

- 克隆在本地使用 markdown 软件打开即可

- git clone git@gitee.com:icanci/MySQL-Review.git

#### 使用说明

- 本项目遵循 [APACHE LICENSE, VERSION 2.0](  http://www.apache.org/licenses/LICENSE-2.0 ) 开源协议
- 开源不易，感谢Star。
- 如果您在阅读过程中发现错误的地方，欢迎指正。

#### 参与贡献

- Fork 本仓库
- 新建 Feat_xxx 分支
- 提交代码
- 新建 Pull Request

#### 文件目录结构树

- 按照此目录结构树食用最佳，如果是MySQL高手，可选择阅读
- 更新中~

##### MySQL基础篇

- [第1篇：MySQL基本概念和安装](./MySQL基础篇/第1篇：MySQL基本概念和安装.md)
- [第2篇：MySQL基本使用和关键字](./MySQL基础篇/第2篇：MySQL基本使用和关键字.md)
- [第3篇：MySQL基本操作语句](./MySQL基础篇/第3篇：MySQL基本操作语句.md)
- [第4篇：MySQL基本查询进阶](./MySQL基础篇/第4篇：MySQL基本查询进阶.md)
- [第5篇：MySQL基本操作更新、插入、删除语句](./MySQL基础篇/第5篇：MySQL基本操作更新、插入、删除语句.md)
- [第6篇：MySQL基本操作创建和操纵表](./MySQL基础篇/第6篇：MySQL基本操作创建和操纵表.md)
- [第7篇：MySQL基本操作视图、存储过程、游标、触发器](./MySQL基础篇/第7篇：MySQL基本操作视图、存储过程、游标、触发器.md)
- [第8篇：MySQL基础事务管理](./MySQL基础篇/第8篇：MySQL基础事务管理.md)
- [第9篇：MySQL全球化、本地化](./MySQL基础篇/第9篇：MySQL全球化、本地化.md)
- [第10篇：MySQL管理用户、数据库维护、改善性能](./MySQL基础篇/第10篇：MySQL管理用户、数据库维护、改善性能.md)

##### MySQL基本练习题篇

- TODO

##### MySQL进阶篇

- TODO

##### MySQL线上实践篇

- TODO

##### MySQL架构刨析篇

- TODO

##### MySQL集群与主从复制高可用篇

- TODO

##### MySQL技术内幕之InnoDB存储引擎篇

- TODO

##### MySQL性能优化篇

- TODO

##### MySQL面试篇

- TODO

##### MySQL分布式架构和NoSQL、NewSQL对比篇

- TODO