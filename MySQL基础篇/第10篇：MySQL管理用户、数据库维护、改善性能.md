### 第10篇：MySQL管理用户、数据库维护、改善性能

#### 管理用户

- MySQL服务器的安全基础是：用户应该对他们需要的数据具有适当的访问权，既不能多也不能少。换句话说，用户不能对过多的数据具有过多的访问权
  - 多数用户只需要对表进行读和写，但是少数用户要可以创建和删除表
  - 某些用户可能需要读表，但是可能不需要更新表
  - 可能想允许用户添加数据，但不允许他们删除数据
  - 某些用户（管理员）可能需要处理用户账号的权限，但多数用户不需要
  - 你可能想让用户通过存储过程访问数据，但不允许他们直接访问数据
  - 可能想根据用户登录的地点限制对某些功能的访问
- **管理用户**

```mysql
USE mysql;
SELECT user FROM user;
```

![1608958603379](images/1608958603379.png)

- mysql数据库有一个名为user的表，它包含所有用户账号。user表有一个名为user的列，它存储用户登录名
- **创建用户账号**

```mysql
CREATE USER ben IDENTIFIED BY 'ppp123';
```

![1608958776789](images/1608958776789.png)

- 重命名一个用户账号

```mysql
RENAME USER ben TO Tom;
```

- **删除用户账号**

```mysql
DROP USER Tom;
```

- **设置访问权限:** 新创建的用户账号没有访问权限。它们能登录MySQL，但是不能执行任何操作。

```mysql
SHOW GRANTS FOR ben;
```

![1608959096147](images/1608959096147.png)

- 上图说明没有任何权限
- 为了设置权限。需要使用GRANT语句
  - 要授予的权限
  - 被授予访问权限的数据库或者表
  - 用户名

```mysql
GRANT SELECT ON mytest.* TO ben;
```

![1608959292510](images/1608959292510.png)

- GRANT的反操作为REVOKE，用它来撤销特定的权限。

```mysql
REVOKE SELECT ON mytest.* FROM ben;
```

![1608959386309](images/1608959386309.png)

- GRANT和REVOKE可在几个层次上控制访问权限
  - 整个服务器，使用GRANTALL和REVOKEALL
  - 整个数据库，使用ON database.*
  - 特定的表，使用ON database.table
  - 特定的列
  - 特定的存储过程
- 表格说明

| 权限                    | 说明                                                         |
| ----------------------- | ------------------------------------------------------------ |
| ALL                     | 除GRANTOPTION外的所有权限                                    |
| ALTER                   | 使用ALTERTABLE                                               |
| ALTER ROUTINE           | 使用ALTERP ROCEDURE和DROP PROCEDURE                          |
| CREATE                  | 使用CREATETABLE                                              |
| CREATE ROUTINE          | 使用CREATE PROCEDURE                                         |
| CREATE TEMPORARY TABLES | 使用CREATE TEMPORARY TABLE                                   |
| CREATE USER             | 使用CREATEUSER、DROPUSER、RENAME USER和REVOKE ALL PRIVILEGES |
| CREATE VIEW             | 使用CREATE VIEW                                              |
| DELETE                  | 使用DELETE                                                   |
| DROP                    | 使用DROP TABLE                                               |
| EXECUTE                 | 使用CALL和存储过程                                           |
| FILE                    | 使用SELECT INTO OUTFILE和LOAD DATA INFILE                    |
| GRANT OPTION            | 使用GRANT和REVOKE                                            |
| INDEX                   | 使用INDEX                                                    |
| LOCK TABLES             | 使用LOCK TABLES                                              |
| RELOAD                  | 使用FLUSH                                                    |
| REPLICATION CLIENT      | 服务器位置访问                                               |
| REPLICATION SLAVE       | 由复制从属使用                                               |
| SELECT                  | 使用SELECT                                                   |
| SHOW DATABASES          | 使用SHOW DATABASES                                           |
| SHOW VIEW               | 使用SHOW CREATE VIEW                                         |
| SHUTDOWN                | 使用mysqladminshutdown（用来关闭MySQL）                      |
| SUPER                   | 使用CHANGEMASTER、KILL、LOGS、PURGE、MASTER和SETGLOBAL。还允许mysqladmin调试登录 |
| UPDATE                  | 使用UPDATE                                                   |
| USAGE                   | 无访问权限                                                   |

- 未来的授权 在使用GRANT和REVOKE时，用户账号必须存在，但对所涉及的对象没有这个要求。这允许管理员在创建数据库和表之前设计和实现安全措施。这样做的副作用是，当某个数据库或表被删除时（用DROP语句），相关的访问权限仍然存在。而且，如果将来重新创建该数据库或表，这些权限仍然起作用。

- 简化多次授权 可通过列出各权限并用逗号分隔，将多条GRANT语句串在一起，如下所示

```mysql
GRANT SELECT , INSERT , ON mytest.* TO ben;
```

- **更改口令（密码）**

```mysql
SET PASSWORD FOR ben = Password('newppp123');
```

#### 数据库维护

- 所有数据库都要经常备份，备份是数据崩溃之后数据恢复的重要依据
  - 使用命令行实用程序mysqldump转储所有数据库内容到某个外部文件。在进行常规备份前这个实用程序应该正常运行，以便能正确地备份转储文件
  - 可用命令行实用程序mysqlhotcopy从一个数据库复制所有数据（并非所有数据库引擎都支持这个实用程序）
  - 可以使用MySQL的BACKUP TABLE或SELECT INTO OUTFILE转储所有数据到某个外部文件。这两条语句都接受将要创建的系统文件名，此系统文件必须不存在，否则会出错。数据可以用RESTORETABLE来复原。
  - 首先刷新未写数据 为了保证所有数据被写到磁盘（包括索引数据），可能需要在进行备份前使用FLUSHTABLES语句。
- **数据库维护**

```mysql
# 检查表键是否正确
ANALYZE TABLE orders;
```

![1608960732664](images/1608960732664.png)

- CHECKTABLE用来针对许多问题对表进行检查。在MyISAM表上还对索引进行检查。CHECKTABLE支持一系列的用于MyISAM表的方式。CHANGED检查自最后一次检查以来改动过的表。EXTENDED执行最彻底的检查，FAST只检查未正常关闭的表，MEDIUM检查所有被删除的链接并进行键检验，QUICK只进行快速扫描

```mysql
CHECK TABLE orders,orderitems;
```

![1608960828742](images/1608960828742.png)

- 如果MyISAM表访问产生不正确和不一致的结果，可能需要用REPAIRTABLE来修复相应的表。这条语句不应该经常使用，如果需要经常使用，可能会有更大的问题要解决
- 如果从一个表中删除大量数据，应该使用OPTIMIZETABLE来收回所用的空间，从而优化表的性能
- **诊断启动问题**
- 服务器启动问题通常在对MySQL配置或服务器本身进行更改时出现。MySQL在这个问题发生时报告错误，但由于多数MySQL服务器是作为系统进程或服务自动启动的，这些消息可能看不到。
- 在排除系统启动问题时，首先应该尽量用手动启动服务器。MySQL服务器自身通过在命令行上执行mysqld启动。下面是几个重要的mysqld命令行选项
  - --help显示帮助 一个选项列表
  - --safe-mode装载减去某些最佳配置的服务器
  - --verbose显示全文本消息（为获得更详细的帮助消息与--help联合使用）
  - --version显示版本信息然后退出
- **查看日志文件**
- MySQL维护管理员依赖的一系列日志文件
  - 错误日志。它包含启动和关闭问题以及任意关键错误的细节。此日志通常名为hostname.err，位于data目录中。此日志名可用--log-error命令行选项更改。
  - 查询日志。它记录所有MySQL活动，在诊断问题时非常有用。此日志文件可能会很快地变得非常大，因此不应该长期使用它。此日志通常名为hostname.log，位于data目录中。此名字可以用--log命令行选项更改。
  - 二进制日志。它记录更新过数据（或者可能更新过数据）的所有语句。此日志通常名为hostname-bin，位于data目录内。此名字可以用--log-bin命令行选项更改。注意，这个日志文件是MySQL5中添加的，以前的MySQL版本中使用的是更新日志。
  - 缓慢查询日志。顾名思义，此日志记录执行缓慢的任何查询。这个日志在确定数据库何处需要优化很有用。此日志通常名为hostname-slow.log，位于data目录中。此名字可以用--log-slow-queries命令行选项更改。
  - 在使用日志时，可用FLUSHLOGS语句来刷新和重新开始所有日志文件

#### 改善性能

- **建议**
  - MySQL（与所有DBMS一样）具有特定的硬件建议。在学习和研究MySQL时，使用任何旧的计算机作为服务器都可以。但对用于生产的服务器来说，应该坚持遵循这些硬件建议。
  - 一般来说，关键的生产DBMS应该运行在自己的专用服务器上
  - MySQL是用一系列的默认设置预先配置的，从这些设置开始通常是很好的。但过一段时间后你可能需要调整内存分配、缓冲区大小等。（为查看当前设置，可使用SHOW VARIABLES;和SHOWSTATUS;。）
  - MySQL一个多用户多线程的DBMS，换言之，它经常同时执行多个任务。如果这些任务中的某一个执行缓慢，则所有请求都会执行缓慢。如果你遇到显著的性能不良，可使用SHOWPROCESSLIST显示所有活动进程（以及它们的线程ID和执行时间）。你还可以用KILL命令终结某个特定的进程（使用这个命令需要作为管理员登录）。
  - 总是有不止一种方法编写同一条SELECT语句。应该试验联结、并、子查询等，找出最佳的方法。
  - 使用EXPLAIN语句让MySQL解释它将如何执行一条SELECT语句。
  - 一般来说，存储过程执行得比一条一条地执行其中的各条MySQL语句快。
  - 应该总是使用正确的数据类型
  - 决不要检索比需求还要多的数据。换言之，不要用SELECT *（除非你真正需要每个列）。
  - 有的操作（包括INSERT）支持一个可选的DELAYED关键字，如果使用它，将把控制立即返回给调用程序，并且一旦有可能就实际执行该操作。
  - 在导入数据时，应该关闭自动提交。你可能还想删除索引（包括FULLTEXT索引），然后在导入完成后再重建它们。
  - 必须索引数据库表以改善数据检索的性能。确定索引什么不是一件微不足道的任务，需要分析使用的SELECT语句以找出重复的WHERE和ORDERBY子句。如果一个简单的WHERE子句返回结果所花的时间太长，则可以断定其中使用的列（或几个列）就是需要索引的对象。
  - 你的SELECT语句中有一系列复杂的OR条件吗？通过使用多条SELECT语句和连接它们的UNION语句，你能看到极大的性能改进。
  - 索引改善数据检索的性能，但损害数据插入、删除和更新的性能。如果你有一些表，它们收集数据且不经常被搜索，则在有必要之前不要索引它们。（索引可根据需要添加和删除。）
  - LIKE很慢。一般来说，最好是使用FULLTEXT而不是LIKE。
  - 数据库是不断变化的实体。一组优化良好的表一会儿后可能就面目全非了。由于表的使用和内容的更改，理想的优化和配置也会改变。
  - 最重要的规则就是，每条规则在某些条件下都会被打破。

- **注意：本部分讲解的内容，只是最简单，最浅显的内容，需要后面专题进行梳理。**

#### 参考

- 《MySQL必知必会》