### 第5篇：MySQL基本操作更新、插入、删除语句

- 在之前的几篇中，主要讲解的就是SELECT语句的操作，现在本篇将学习另外3个操作，数据插入、更新和删除

#### 数据插入

- INSERT 是用来插入一行或者多行数据到数据库表的，插入可以用几种方式使用
  - 插入完整的行
  - 插入行的一部分
  - 插入多行
  - 插入某些查询的结果
- 可以针对每个表或者每个用户，利用MySQL的安全机制禁止使用INSERT语句，后面会提到
- **插入完整的行：** 一般没有输出

```mysql
INSERT INTO Customers VALUES(NULL,'Pep E. LaPew','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
```

![1608596147545](images/1608596147545.png)

- 编写INSERT语句更繁琐，也更安全的写法

```mysql
INSERT INTO customers(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) VALUES('Pep E. LaPew','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
```

![1608596319080](images/1608596319080.png)

- 这种方式更加安全，可以给出每一个列的值
- 不管使用哪种INSERT语法，都必须给出VALUES的正确数目。如果不提供列名，则必须给每个表列提供一个值。如果提供列名，则必须对每个列出的列给出一个值。如果不这样，将产生一条错误消息，相应的行插入不成功。
- 省略列 
  - 如果表的定义允许，则可以在INSERT操作中省略某些列。省略的列必须满足以下某个条件。
  - 该列定义为允许NULL值（无值或空值）。
  - 在表定义中给出默认值。这表示如果不给出值，将使用默认值。
- **提高整体性能** 
  - 数据库经常被多个客户访问，对处理什么请求以及用什么次序处理进行管理是MySQL的任务。INSERT操作可能很耗时（特别是有很多索引需要更新时），而且它可能降低等待处理的SELECT语句的性能。
  - 如果数据检索是最重要的（通常是这样），则你可以通过在INSERT和INTO之间添加关键字LOW_PRIORITY，指示MySQL降低INSERT语句的优先级，如下所示
  - INSERT LOW_PRIORIPY INTO
  - UPDATE 和 DELETE 也适用
- **插入多个行**
- INSERT INTO 可以插入一行数据，如果需要添加多行，可以使用多条INSERT 语句，使用分号隔开

```mysql
INSERT INTO customers(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) VALUES('Pep E. LaPew1','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
INSERT INTO customers(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) VALUES('Pep E. LaPew2','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
INSERT INTO customers(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) VALUES('Pep E. LaPew3','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
INSERT INTO customers(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) VALUES('Pep E. LaPew4','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
```

![1608596655115](images/1608596655115.png)

- 多条合并插入 其中单条INSERT语句有多组值，每组值用一对圆括号括起来，用逗号分隔。

```mysql
INSERT INTO customers(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) VALUES('Pep E. LaPew6','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL),('Pep E. LaPew7','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL),('Pep E. LaPew8','100 Main Street','Los Angeles','CA','90045','USA',NULL,NULL);
```

![1608596761016](images/1608596761016.png)

- 提高INSERT的性能 此技术可以提高数据库处理的性能，因为MySQL用单条INSERT语句处理多个插入比使用多条INSERT语句快。
- **插入检索出的数据：** INSERT一般用来给表插入一个指定列值的行。但是，INSERT还存在另一种形式，可以利用它将一条SELECT语句的结果插入表中。这就是所谓的INSERT SELECT，顾名思义，它是由一条INSERT语句和一条SELECT语句组成的。
- 先创建一张一样的表，和 customers 一致

```mysql
CREATE TABLE custnew
(
    cust_id      int       NOT NULL AUTO_INCREMENT,
    cust_name    char(50)  NOT NULL ,
    cust_address char(50)  NULL ,
    cust_city    char(50)  NULL ,
    cust_state   char(5)   NULL ,
    cust_zip     char(10)  NULL ,
    cust_country char(50)  NULL ,
    cust_contact char(50)  NULL ,
    cust_email   char(255) NULL ,
    PRIMARY KEY (cust_id)
) ENGINE=InnoDB;
```

![1608596924711](images/1608596924711.png)

- INSERT INTO SELECT 语句

```mysql
INSERT INTO custnew(cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email) SELECT cust_name,cust_address,cust_city,cust_state,cust_zip,cust_country,cust_contact,cust_email FROM customers;
```

![1608597047839](images/1608597047839.png)

- INSERT SELECT中的列名 为简单起见，这个例子在INSERT和SELECT语句中使用了相同的列名。但是，不一定要求列名匹配。事实上，MySQL甚至不关心SELECT返回的列名。它使用的是列的位置，因此SELECT中的第一列（不管其列名）将用来填充表列中指定的第一个列，第二列将用来填充表列中指定的第二个列，如此等等。这对于从使用不同列名的表中导入数据是非常有用的。

#### 数据更新

- 为了更新（修改）表中的数据，可使用UPDATE语句。可采用两种方式使用UPDATE
  - 更新表中的特定的行
  - 更新表中所有行
- 在使用 UPDATE的时候，不要省略WHERE子句。在使用UPDATE时一定要注意细心。因为稍不注意，就会更新表中所有行。
- UPDATE语句非常容易使用，甚至可以说是太容易使用了。基本的UPDATE语句由3部分组成，分别是
  - 要更新的表
  - 列名和他们的值
  - 确定要更新行的过滤条件

```mysql
# 先查询一下
SELECT * FROM customers WHERE cust_id = 10005;
# 更新操作
UPDATE customers SET cust_email = 'email@ffxx.com' WHERE cust_id = 10005;
```

![1608597351071](images/1608597351071.png)

- 更新多个列

```mysql
# 先查询一下
SELECT * FROM customers WHERE cust_id = 10005;
# 更新操作
UPDATE customers SET cust_name = 'hello',cust_email = 'hello@ffxx.com' WHERE cust_id = 10005;
```

![1608597438233](images/1608597438233.png)

- 在UPDATE语句中使用子查询 UPDATE语句中可以使用子查询，使得能用SELECT语句检索出的数据更新列数据
- IGNORE关键字 如果用UPDATE语句更新多行，并且在更新这些行中的一行或多行时出一个现错误，则整个UPDATE操作被取消（错误发生前更新的所有行被恢复到它们原来的值）。为即使是发生错误，也继续进行更新，可使用IGNORE关键字，如下所示： UPDATE IGNORE customers…\
- 为了删除某个列的值，可设置它为NULL（假如表定义允许NULL值）。

```mysql
UPDATE customers SET cust_email = NULL WHERE cust_id = 10005;
```

![1608597554190](images/1608597554190.png)

#### 删除数据

- 为了从一个表中删除（去掉）数据，使用DELETE语句。可以两种方式使用DELETE
  - 从表中删除特定的行
  - 从表中删除所有行
- 不要省略WHERE子句 在使用DELETE时一定要注意细心。因为稍不注意，就会错误地删除表中所有行。

```mysql
# 删除一行
DELETE FROM customers WHERE cust_id = 10006;
```

![1608597679948](images/1608597679948.png)

- 删除表的内容而不是表 DELETE语句从表中删除行，甚至是删除表中所有行。但是，DELETE不删除表本身。
- 更快的删除 如果想从表中删除所有行，不要使用DELETE。可使用TRUNCATE TABLE语句，它完成相同的工作，但速度更快（TRUNCATE实际是删除原来的表并重新创建一个表，而不是逐行删除表中的数据）。

- **更新和删除的指导原则**
  - 除非确实打算更新和删除每一行，否则绝对不要使用不带WHERE子句的UPDATE或DELETE语句。
  - 保证每个表都有主键，尽可能像WHERE子句那样使用它（可以指定各主键、多个值或值的范围）。
  - 在对UPDATE或DELETE语句使用WHERE子句前，应该先用SELECT进行测试，保证它过滤的是正确的记录，以防编写的WHERE子句不正确。
  - 使用强制实施引用完整性的数据库，这样MySQL将不允许删除具有与其他表相关联的数据的行。
  - 小心使用 MySQL没有撤销（undo）按钮。应该非常小心地使用UPDATE和DELETE，否则你会发现自己更新或删除了错误的数据。**这个地方会有数据恢复和备份，之后的章节会讲到**

#### 参考

- 《MySQL必知必会》



