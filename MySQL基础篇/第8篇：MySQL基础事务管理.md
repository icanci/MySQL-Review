### 第8篇：MySQL基础事务管理

#### 事务处理

- 事务是什么，事务是一种原子操作，在这个原子操作中所执行的内容，要么全部成功，要么全部失败。**数据库事务的实现原理，在之后的篇章会进行说明讲解，本篇只进行基本使用的演示**
- 事务是Inndb引擎执行的最小单位
- **并非所有的引擎都支持事务处理**
  - MySQL支持多种数据库引擎，其中常用的执行引擎有MyISAM和InnoDB，其中InnoDB支持事务
- 务处理（transaction processing）可以用来维护数据库的完整性，它保证成批的MySQL操作要么完全执行，要么完全不执行。
- 例如：给系统添加订单的过程如下
  - 检查数据库中是否存在相应的客户（从customers表查询），如果不存在，添加他/她。
  - 检索客户的 id
  - 添加一行到orders表，把它与客户id想关联
  - 检索orders表中赋予的新订单id
  - 对于订购的每个物品在orderitems表中添加一行，通过检索出来的id把它与orders表关联（以及通过产品ID与products表关联）
- 假如上述的过程某一步出现了问题 ，这个过程没有完成，那么就会出现数据不一致的问题
- 事务处理：
  - 检查数据库中是否存在相应的客户，如果不存在，添加他/她。
  - 提交客户信息
  - 检索客户的ID
  - 添加一行到orders表
  - 如果在添加行道orders表时候出现故障，回退。
  - 检索orders表中赋予的新订单id
  - 对于订购的每项物品，添加新行到orderitems表
  - 如果在添加新行到orderitems时出现故障，回退所有添加的orderitems行和orders行
  - 提交订单信息
- 使用事务处理的时候，有几个关键词反复出现
  - **事务（transaction）** ：是指一组SQL语句
  - **回退（rollback）** ：是指撤销指定SQL语句的过程
  - **提交（commit）** ：指将未存储的SQL语句结果写入数据库表；
  - **保留点（）savepoint** ：指事务处理中设置的临时占位符（place-holder） ，你可以对它发布回退（与整个事务处理不同）
- **控制事务管理**
- 管理事务的关键在于将SQL语句分解为逻辑块，并明确规定数据何是应该回退，何是应该不回退
- MySQL使用下面的语句来标识事务的开始

```mysql
START TRANSACTION
```

- 使用ROLLBACK
- MySQL 的ROLLBACK 命令用来回退（撤销） MySQL语句

```mysql
SELECT * FROM ordertotals;
START TRANSACTION ;
SELECT * FROM ordertotals;
SELECT * FROM ordertotals;
ROLLBACK;
SELECT * FROM ordertotals;
```

![1608951140032](images/1608951140032.png)

- 上面这个例子显示ordertotals表的内容，然后开始事务，然后删除数据，显示数据，回滚数据，在进行查询数据。
- 可以回退的语句：DELETE、INSERT、UPDATE，不可以回退CREATE操作和DROP操作
- **使用COMMIT**
- 一般的MySQL语句都是直接针对数据库表执行和编写的。这就是所谓的隐含提交（implicitcommit），即提交（写或保存）操作是自动进行的
- 在事务处理中，事务不会自动提交，此时需要手动提交

```mysql
START TRANSACTION;
DELETE FROM orderitems WHERE order_num = 20010;
DELETE FROM orders WHERE order_num = 20010;
COMMIT;
```

- **使用保留点**
- 每个保留点都标识它唯一的名字，以便在回退的时候，MySQL知道要回到何处

```mysql
ROLLBACK TO deletel;
```

- 保留点越多越好 可以在MySQL代码中设置任意多的保留点，越多越好。为什么呢？因为保留点越多，你就越能按自己的意愿灵活地进行回退。
- 释放保留点 保留点在事务处理完成（执行一条ROLLBACK或COMMIT）后自动释放。自MySQL 5以来，也可以用RELEASESAVEPOINT明确地释放保留点。
- **更改事务默认的提交行为**
- 默认的MySQL行为是自动提交所有的更改。换句话说，任何时候你执行一条MySQL的时候，该语句实际上都是针对表执行的，而且所做的更改立即生效。为指示MySQL不自动提交更改，设置autocommit为0（假）指示MySQL不自动提交更改，直到autocommit 被设置为真为止
- autocommit标志是针对每个连接而不是针对服务器的

- **注意：本篇讲解的这是MySQL的基本事务操作，关于MySQL的更加深刻的理解将会在之后的比较中给出**

#### 参考

- 《MySQL必知必会》