### 第2篇：MySQL基本使用和关键字

- 备注：我现在使用的是Window10操作系统，版本为5.7 的MySQL数据库

#### 连接数据库

- 如果你使用的是命令行窗口，连接到数据库只需要如下命令即可，前提是你是连接的本地数据库并且端口号是默认的 3306

```mysql
mysql -uroot -proot
```

- 如果不是，则应该这样连接

```mysql
mysql -uroot -p3306 -hlocalhost -proot
# mysql -u账户 -p端口号 -h主机地址 -p密码
```

![1608337611623](images/1608337611623.png)

#### 显示数据库中的表

- 命令如下

```mysql
show databases;
```

![1608337668022](images/1608337668022.png)

- 使用一个数据库，命令如下

```mysql
use guli;
# 会输出 Database changed
# 此时说明已经切换到 guli 数据库了
```

![1608337723766](images/1608337723766.png)

- 查看数据库中的表

```mysql
show tables;
```

![1608337827595](images/1608337827595.png)

- show 命令也可以用来显示表的列

```mysql
show columns from t_order;
# show columns from 表名;
# 它会对每个字段返回一行，其中包含字段名、数据类型、是否可以为NULL、Key列的键的类型、默认值、其他信息
```

![1608337926817](images/1608337926817.png)

- 也可以使用以下命令获得和上面一样的结果

```mysql
describe t_orders;
```

![1608338123968](images/1608338123968.png)

- 其他的show的命令

```mysql
# 用户显示广泛的服务器状态信息，会有很多输出
show status; 
# 显示创建特定数据库和表的MySQL语句
show create database 数据库名字;
show create table 表名字;
# 显示授予用户（所有用户和特定用户）的安全权限
show grants;
# 显示服务器错误信息和警告信息
show errors;
show warnings;
# 进一步了解SHOW 请在mysql命令行实用程序中，执行命令HELP SHOW;显示允许的SHOW语句。
```

![1608338325773](images/1608338325773.png)

![1608338405529](images/1608338405529.png)

![1608338472867](images/1608338472867.png)

#### 关键字[部分]

- use
- show 
- describe
- from 
- create
- database

#### 数据库创建语句和创建表语句

- 首先使用一个简单的命令创建一个数据库，用来学习和测试

```mysql
create database mytest;
```

![1608338697045](images/1608338697045.png)

- 创建一张表[具体的之后再解释]

```mysql
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键无意义',
  `username` varchar(255) NOT NULL COMMENT '姓名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

![1608338942378](images/1608338942378.png)

- 向数据表test中插入一些数据，方便测试[具体的之后再解释]

```mysql
insert into test(username,password,create_time) values ('hello','hello_password',NOW());
insert into test(username,password,create_time) values ('hello1','hello_password1',NOW());
insert into test(username,password,create_time) values ('hello2','hello_password3',NOW());
insert into test(username,password,create_time) values ('hello3','hello_password3',NOW());
```

![1608339236221](images/1608339236221.png)

#### 数据库检索语句

- select 关键字，在test表中查询列名为 username 的所有的值
- 注意：SQL语句不区分大小写，下面的两行操作的结果是一样的
- 建议：关键字大写，非关键字小写，便于区分

```mysql
select username from test;
SELECT username FROM test;
```

![1608339272530](images/1608339272530.png)

![1608339418533](images/1608339418533.png)

- 检索多个列
- 注意：检索的列之间，需要使用英文的 逗号隔开，最后一个后面不需要逗号，下面的语句都是合法的

```mysql
SELECT id,username,password,create_time FROM test;
SELECT id,username,password FROM test;
SELECT id,username FROM test;
```

![1608339522316](images/1608339522316.png)

- 检索所有的列

```mysql
# 如果给定一个通配符（*）
# 使用通配符 一般，除非你确实需要表中的每个列，否则最好别使用*通配符。虽然使用通配符可能会使你自己省事，不用明确列出所需列，但检索不需要的列通常会降低检索和应用程序的性能。
# 索未知列 使用通配符有一个大优点。由于不明确指定列名（因为星号检索每个列），所以能检索出名字未知的列。

SELECT * FROM test;

# 等价于 SELECT id,username,password,create_time FROM test;
```

![1608341625511](images/1608341625511.png)

- 在上面的数据中，有一行是重复的名字 hello，如执行以下SQL语句的结果，有多个一样的，此处只想要不一样的，应该使用关键字 DISTINCT 去重

```mysql
SELECT username FROM test;
SELECT DISTINCT username FROM test;
```

![1608341843357](images/1608341843357.png)

- 限制结果，也就是限制返回的结果和分页

```mysql
# 查询2条
SELECT username FROM test limit 2;
# 查询3条
SELECT username FROM test limit 3;
```

![1608379062356](images/1608379062356.png)

- 限制结果

```mysql
# LIMIT 2,2 指示MySQL从行2开始返回2行
SELECT username FROM test limit 2,2;
# 注意，在行数不够的时候，会返回仅有的行
```

![1608379165254](images/1608379165254.png)

- 使用完全限定的表名查询

```mysql
SELECT test.username FROM test;
```

![1608379273102](images/1608379273102.png)

- 当然，表名也是可以限制的

```mysql
SELECT test.username FROM mytest.test;
```

![1608379358679](images/1608379358679.png)

#### 参考

- 《MySQL必知必会》