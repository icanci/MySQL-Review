### 第9篇：MySQL全球化、本地化

#### 字符集和校对顺序

- 数据库表被用来存储和检索数据，不同的语言和字符集需要以不同的方式存储和检索，因此，MySQL需要适应不同的字符集（不同的字母和组合），适用不同的排序和检索数据的方法。
- **字符集：** 为字母和符号的组合
- **编码：** 为某个字符集成员的内部表示
- **校对：** 为规定字符如何比较的指令
- **使用字符集和校验顺序**

```mysql
SHOW CHARACTER SET;
```

![1608957291025](images/1608957291025.png)

- 这条语句显示所有可用的字符集以及每个字符集的描述和默认校对

```mysql
# 为了查看所支持校对的完整列表，使用以下语句
SHOW COLLATION;
# 截图不放上来了

# 此语句显示所有可用的校对，以及它们适用的字符集。可以看到有的字符集具有不止一种校对。例如，latin1对不同的欧洲语言有几种校对，而且许多校对出现两次，一次区分大小写（由_cs表示），一次不区分大小写（由_ci表示）
```

- 此外，也可以在创建数据库时，指定默认的字符集和校对。为了确定所用的字符集和校对，可以使用以下语句

```mysql
SHOW VARIABLES LIKE 'character%';
SHOW VARIABLES LIKE 'collations%';
```

![1608957522437](images/1608957522437.png)

- 创建表的时候指定字符集和校对

```mysql
CREATE TABLE mytable(col1 INT, col2 VARCHAR(10)) DEFAULT CHARACTER SET hebrew COLLATE hebrew_general_ci; 
# 这个例子中指定了CHARACTERSET和COLLATE两者。一般，MySQL如下确定使用什么样的字符集和校对
# 如果指定CHARACTERSET和COLLATE两者，则使用这些值
# 如果只指定CHARACTERSET，则使用此字符集及其默认的校对（如SHOWCHARACTERSET的结果中所示）
# 如果既不指定CHARACTERSET，也不指定COLLATE，则使用数据库默认
```

- MySQL 也支持每个列设置的字符集不同

```mysql
CREATE TABLE mytable(col1 INT,col2 VARCHAR(10),col3 VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_general_ci) DEFAULT CHARACTER SET hebrew COLLATE hebrew_general_ci;
# 这里对整个表以及一个特定的列指定了CHARACTERSET和COLLATE
```

#### 参考

- 《MySQL必知必会》