### 第7篇：MySQL基本操作视图、存储过程、游标、触发器

#### 视图

- 需要MySQL5以上的版本 MySQL5添加了对视图的支持。如果你的版本低于此版本，将不能使用其功能
- 视图是虚拟的表，与包含数据的表不一样，视图只包含使用时动态检索数据的查询。
- 举例

```mysql
# 使用 SELECT 语句检索数据
SELECT cust_name,cust_contact FROM customers,orders,orderitems WHERE customers.cust_id = orders.cust_id AND orderitems.order_num = orders.order_num AND prod_id = 'TNT2';
```

![1608682836094](images/1608682836094.png)

- 现在，假如可以把整个查询包装成一个名为productcustomers的虚拟表，则可以如下轻松地检索出相同的数据

```mysql
SELECT cust_name,cust_contact FROM productcustomers WHERE prod_id = 'TNT2';
```

- 这就是视图的作用。productcustomers是一个视图，作为视图，它不包含表中应该有的任何列或数据，它包含的是一个SQL查询
- **为什么要使用视图？**
  - 重用SQL语句
  - 简化复杂的SQL操作。在编写查询之后，可以方便的重用它而不必知道它的基本查询细节
  - 使用表的组成部分而不是整个表
  - 保护数据。可以给用户授予表的特定部分的访问权限而不是整个表的访问权限
  - 更改数据格式和表示。视图可以返回与底层表的表示和格式不同的数据
- 在视图创建之后，可以用与表基本相同的方式利用它们。可以对视图执行SELECT操作，过滤和排序数据，将视图联结到其他视图或表，甚至能添加和更新数据（添加和更新数据存在某些限制。关于这个内容稍后还要做进一步的介绍）
- **注意：** 重要的是知道视图仅仅是用来查看存储在别处的数据的一种设施。视图本身不包含数据，因此它们返回的数据是从其他表中检索出来的。在添加或更改这些表中的数据时，视图将返回改变过的数据。
- **视图的规则和限制**
  - 与表一样，视图的名字必须是唯一的（不能给视图取与别的视图或者表相同的名字）
  - 对于可以创建的视图的个数没有限制
  - 为了创建视图，必须具有足够的访问权限。这些限制通常由数据管理员授予。
  - 视图可以嵌套，即可以利用从其他视图中检索的数据的查询来构造一个视图
  - ORDER BY 可以用在视图中，但是如果从该视图中检索数据SELECT中也包含ORDER BY ，那么该视图中的ORDER BY 将被覆盖
  - 视图不能索引，也不能有关联的触发器或者默认值
  - 视图可以和表一起使用。例如，编写一条联结表和视图的SELECT语句。
- **使用视图**
  - 视图使用 CREATE VIEW 语句来创建
  - 使用 SHOW CREATE VIEW viewname; 来查看创建视图的语句
  - 用DROP删除视图，其删除语法为：DROP VIEW viewname;
  - 更新视图时，可以先用DROP再用CREATE，也可以直接用CREATE OR REPLACE VIEW。如果要更新的视图不存在，则第2条更新语句会创建一个视图；如果要更新的视图存在，则第2条更新语句会替换原有视图。
- 利用视图简化复杂的联结

```mysql
CREATE VIEW productcustomers AS SELECT cust_name,cust_contact,prod_id FROM customers,orders,orderitems WHERE customers.cust_id = orders.cust_id AND orderitems.order_num = orders.order_num;
```

- 这条语句创建一个名为productcustomers的视图，它联结三个表，以返回已订购了任意产品的所有客户的列表。如果执行SELECT * FROM productcustomers，将列出订购了任意产品的客户。
- 查询视图

```mysql
SELECT cust_name,cust_contact FROM productcustomers WHERE prod_id = 'TNT2';
```

![1608683796397](images/1608683796397.png)

- **用视图重新格式化检索出的数据**

```mysql
SELECT Concat(RTrim(vend_name),'(',RTrim(vend_country),')') AS vend_title FROM vendors ORDER BY vend_name;
```

- 现在，假如经常需要这个格式的结果。不必在每次需要时执行联结，创建一个视图，每次需要时使用它即可。为把此语句转换为视图，可按如下进行

```mysql
CREATE VIEW vendorslocation AS SELECT Concat(RTrim(vend_name),'(',RTrim(vend_country),')') AS vend_title FROM vendors ORDER BY vend_name;
```

![1608684174401](images/1608684174401.png)

- **使用视图过滤不需要的数据**

```mysql
# 创建视图
CREATE VIEW customeremaillist AS SELECT cust_id,cust_name,cust_email FROM customers WHERE cust_email IS NOT NULL;
# 使用视图
SELECT * FROM customeremaillist;
```

![1608761352209](images/1608761352209.png)

- **使用视图计算字段**

```mysql
SELECT prod_id,quantity,item_price,quantity*item_price AS expanded_price FROM orderitems WHERE order_num = 20005;
# 创建视图
CREATE VIEW orderitemsexpanded AS SELECT prod_id,quantity,item_price,quantity*item_price AS expanded_price FROM orderitems WHERE order_num = 20005;
# 查询视图
SELECT * FROM orderitemsexpanded;
```

![1608761532628](images/1608761532628.png)

- **更新视图**
- 视图时可以更新的（可以进行INSERT、UPDATE、DELETE操作），更新一个视图将更新其基表（因为视图本身是没有数据的）。如果你对视图进行增加和删除，实际上是对其基表进行增加和删除的。
- 但是并非所有的视图都是可以更新的，如果MySQL不能正确的确定被更新的基本数据，则不允许更新（包括插入和删除）。也就是说，如果视图中有这些操作，就不能进行更新
  - 分组（使用HAVING和GROUP BY）
  - 联结
  - 子查询
  - 并
  - 聚集函数（MIN(),MAX(),Count(),Sum()等）
  - DISTINCT
  - 导出（计算）列
- 换句话说，很多种视图是不可以更新的，实际上，视图的最大作用就是用来查询数据和检索数据的。
- 视图是虚拟的表，**“类似于把SQL语句做了一层封装”**

#### 使用存储过程

- 存储过程在MySQL5之后支持

- 在实际开发中，经常会有一个完整的操作需要执行多条语句才能完成，举例

  - 为了处理订单，需要核对以保证库存中有相应的物品
  - 如果库存有物品，这些物品需要预定以便不将它们再卖给别的人，并且要减少可用的物品数量以反映正确的库存量
  - 库存中没有的物品要进行订购，这需要与供应商进行某种交互
  - 关于哪些物品入库（并且可以立即发货）和哪些物品退订，需要通知相应的客户

- **为什么需要创建存储过程**

  - 通过把处理封装在容易使用的单元中，简化复杂的操作（正如前面例子所述）。
  - 由于不要求反复建立一系列处理步骤，这保证了数据的完整性。如果所有开发人员和应用程序都使用同一（试验和测试）存储过程，则所使用的代码都是相同的。这一点的延伸就是防止错误。需要执行的步骤越多，出错的可能性就越大。防止错误保证了数据的一致性。
  - 简化对变动的管理。如果表名、列名或业务逻辑（或别的内容）有变化，只需要更改存储过程的代码。使用它的人员甚至不需要知道这些变化。这一点的延伸就是安全性。通过存储过程限制对基础数据的访问减少了数据讹误（无意识的或别的原因所导致的数据讹误）的机会。
  - 提高性能。因为使用存储过程比使用单独的SQL语句要快。
  - 存在一些只能用在单个请求中的MySQL元素和特性，存储过程可以使用它们来编写功能更强更灵活的代码。
  - 换句话说，使用存储过程有3个主要的好处，即简单、安全、高性能。显然，它们都很重要。
  - **不过，在将SQL代码转换为存储过程前，也必须知道它的一些缺陷**
    - 一般来说，存储过程的编写比基本SQL语句复杂，编写存储过程需要更高的技能，更丰富的经验
    - 你可能没有创建存储过程的安全访问权限。许多数据库管理员限制存储过程的创建权限，允许用户使用存储过程，但不允许他们创建存储过程

- **使用存储过程**

- **执行存储过程**

- MySQL称存储过程的执行为调用，因此MySQL执行存储过程的关键字为 CALL。CALL接收存储过程的名字以及需要传递给它的任意参数

 ```mysql
CALL productpricing(@pricelow,@pricehigh,@priceaverage);
# 执行名字为 productpricing 的存储过程，计算返回产品的最低、最高和平均价格
 ```

- 创建存储过程

```mysql
# 当然，这种方式创建在命令行情况下会报错
CREATE PROCEDURE productpricing() BEGIN SELECT Avg(prod_price) AS priceaverage FROM products; END;
# 解决方法
DELIMITER //
CREATE PROCEDURE productpricing() BEGIN SELECT Avg(prod_price) AS priceaverage FROM products;
END //
DELIMITER ;
```

![1608763693054](images/1608763693054.png)

- 此存储过程名为productpricing，用CREATE PROCEDURE productpricing()语句定义。如果存储过程接受参数，它们将在()中列举出来。此存储过程没有参数，但后跟的()仍然需要。BEGIN和END语句用来限定存储过程体，过程体本身仅是一个简单的SELECT语句
- 默认的MySQL语句分隔符为 **;** 如果命令行实用程序要解释存储过程自身内的;字符，则它们最终不会成为存储过程的成分，这会使存储过程中的SQL出现句法错误。解决方式如上代码。**DELIMITER //** 告诉命令行实用程序使用//作为新的语句结束分隔符。可以看到标志存储过程结束的END定义为**END //** 而不是 **END;**
- **调用存储过程**

```mysql
CALL productpricing();
```

![1608763843534](images/1608763843534.png)

- 删除存储过程

```mysql
DROP PROCEDURE productpricing;
# 注意：如果指定的过程不存在，则DROP PROCEDURE将产生一个错误。当过程存在想删除它时（如果过程不存在也不产生错误）可使用DROP PROCEDURE IF EXISTS
DROP PROCEDURE IF EXISTS productpricing;
```

- **使用参数**
- 变量（variable）内存中一个特定的位置，用来临时存储数据

```mysql
# 创建存储过程
DELIMITER //
CREATE PROCEDURE productpricing(OUT pl DECIMAL(8,2),OUT ph DECIMAL(8,2),OUT pa DECIMAL(8,2)) BEGIN SELECT Min(prod_price) INTO pl FROM products;SELECT Max(prod_price) INTO ph FROM products;SELECT Avg(prod_price) INTO pa FROM products;
END //
DELIMITER ;
# 调用存储过程，必须指定3个参数名字
CALL productpricing(@pricelow,@pricehigh,@priceaverage);
# 因为这个存储过程要去3个参数，因此只能传递3个，必须传递。这3个参数，是存储过程将保存结果的3个名字
# 变量名 所有的MySQL变量都必须以@开始
# 查询执行之后的结果
SELECT @pricelow;
SELECT @pricehigh;
SELECT @priceaverage;
```

- 此存储过程接受3个参数：pl存储产品最低价格，ph存储产品最高价格，pa存储产品平均价格。每个参数必须具有指定的类型，这里使用十进制值。关键字OUT指出相应的参数用来从存储过程传出一个值（返回给调用者）。MySQL支持IN（传递给存储过程）、OUT（从存储过程传出，如这里所用）和INOUT（对存储过程传入和传出）类型的参数。存储过程的代码位于BEGIN和END语句内，如前所见，它们是一系列SELECT语句，用来检索值，然后保存到相应的变量（通过指定INTO关键字）。

![1608764356946](images/1608764356946.png)

- 举例

```mysql
# 创建存储过程
DELIMITER //
CREATE PROCEDURE ordertotal(IN onumber INT,OUT ototal DECIMAL(8,2)) BEGIN SELECT Sum(item_price*quantity) FROM orderitems WHERE order_num = onumber INTO ototal;
END //
DELIMITER ;
# 调用存储过程
CALL ordertotal(20005,@total);
SELECT @total;
# 注意点：MySQL在创建存储过程的时候不回去检查里面的SQL语句是否正确，在执行的时候才回去检查
```

![1608764939439](images/1608764939439.png)

#### 使用触发器

- 触发器是MySQL5之后提供的
- MySQL语句在需要时被执行，存储过程也是如此。但是，如果你想要某条语句（或某些语句）在事件发生时自动执行，怎么办呢
  - 每当增加一个顾客到某个数据库表时，都检查其电话号码格式是否正确，州的缩写是否为大写
  - 每当订购一个产品时，都从库存数量中减去订购的数量
  - 无论何时删除一行，都在某个存档表中保留一个副本
- 所有这些例子的共同之处是它们都需要在某个表发生更改时自动处理。这确切地说就是触发器。触发器是MySQL响应以下任意语句而自动执行的一条MySQL语句（或位于BEGIN和END语句之间的一组语句）
  - DELETE
  - INSERT
  - UPDATE
- **创建触发器：** 在创建触发器的时候，需要给出4条信息
  - 唯一的触发器名字
  - 触发器关联的表
  - 触发器应该响应的活动（DELETE、INSERT、UPDATE）
  - 触发器何是执行（处理之前或者处理之后）
- 触发器用CREATE TRIGGER语句创建。

```mysql
# 此行语句在MySQL5.x的早起版本支持，现在我的版本是5.7.x 所以应该使用下面的语句 
# 参考文章在文末
CREATE TRIGGER newproduct AFTER INSERT ON products FOR EACH ROW SELECT 'Product added';
# 新版本
CREATE TRIGGER newproduct AFTER INSERT ON products FOR EACH ROW SELECT 'Product added' INTO @args;
```

- CREATETRIGGER用来创建名为newproduct的新触发器。触发器可在一个操作发生之前或之后执行，这里给出了AFTERINSERT，所以此触发器将在INSERT语句成功执行后执行。这个触发器还指定FOREACHROW，因此代码对每个插入行执行。在这个例子中，文本Product added将存储到 @args 中去
- 仅支持表 只有表才支持触发器，视图不支持（临时表也不支持）。
- 每个表每个事件每次只允许一个触发器。因此，每个表最多支持6个触发器（每条INSERT、UPDATE和DELETE的之前和之后）。单一触发器不能与多个事件或多个表关联，所以，如果你需要一个对INSERT和UPDATE操作执行的触发器，则应该定义两个触发器。

```mysql
SELECT @args;
INSERT INTO products VALUES('pro_xx_01',1001,'icanci',2.88,'hello icanci');
SELECT @args;
```

![1608769998116](images/1608769998116.png)

- **触发器的删除**
- DROP TRIGGER newproduct; 
- 触发器不能更新或覆盖。为了修改一个触发器，必须先删除它，然后再重新创建。

- **INSERT 触发器**
- INSERT触发器在INSERT语句执行之前或之后执行。需要知道以下几点
  - 在INSERT触发器代码内，可引用一个名为NEW的虚拟表，访问被插入的行
  - 在BEFOREINSERT触发器中，NEW中的值也可以被更新（允许更改被插入的值）；
  - 对于AUTO_INCREMENT列，NEW在INSERT执行之前包含0，在INSERT执行之后包含新的自动生成值

- **DELETE 触发器**
- DELETE触发器在DELETE语句执行之前或者之后执行，需要知道以下2点
  - 在DELETE触发器代码内，你可以引用一个名为OLD的虚拟表，访问被删除的行
  - OLD中的值都是只读的，不能更新

```mysql
DELIMITER //
CREATE TRIGGER deleteorder BEFORE DELETE ON orders FOR EACH ROW 
BEGIN INSERT INTO archive_orders(order_num,order_date,cust_id) VALUES (OLD.order_num,OLD.order_date,OLD.cust_id); 
END //
DELIMITER ;
```

- 在任意订单被删除前将执行此触发器。它使用一条INSERT语句将OLD中的值（要被删除的订单）保存到一个名为archive_ orders的存档表中（为实际使用这个例子，你需要用与orders相同的列创建一个名为archive_orders的表）
- 使用BEFORE DELETE触发器的优点（相对于AFTER DELETE触发器来说）为，如果由于某种原因，订单不能存档，DELETE本身将被放弃
- 多语句触发器 正如所见，触发器deleteorder使用BEGIN和END语句标记触发器体。这在此例子中并不是必需的，不过也没有害处。使用BEGINEND块的好处是触发器能容纳多条SQL语句（在BEGINEND块中一条挨着一条）。
- **UPDATE 触发器**
- UPDATE触发器在UPDATE语句执行之前或之后执行。需要知道以下几点
  - 在UPDATE触发器代码中，你可以引用一个名为OLD的虚拟表访问以前（UPDATE语句前）的值，引用一个名为NEW的虚拟表访问新更新的值；
  - 在BEFOREUPDATE触发器中，NEW中的值可能也被更新（允许更改将要用于UPDATE语句中的值）；
  - OLD中的值全部都是只读的，不能更新

```mysql
CREATE TRIGGER updatevendor BEFORE UPDATE ON vendors FOR EACH ROW SET NEW.vend_state = Upper(NEW.vend_state);
```

- 任何UPDATE语句执行之前进行执行。
- **触发器的进一步了解**
  - 创建触发器可能需要特殊的安全访问权限，但是，触发器的执行是自动的。如果INSERT、UPDATE或DELETE语句能够执行，则相关的触发器也能执行。
  - 应该用触发器来保证数据的一致性（大小写、格式等）。在触发器中执行这种类型的处理的优点是它总是进行这种处理，而且是透明地进行，与客户机应用无关。
  - 触发器的一种非常有意义的使用是创建审计跟踪。使用触发器，把更改（如果需要，甚至还有之前和之后的状态）记录到另一个表非常容易。

#### 参考

- 《MySQL必知必会》
- 触发器用CREATE TRIGGER语句创建报错解决方式：https://blog.csdn.net/Mr_Programming_Liu/article/details/89376988