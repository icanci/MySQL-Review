### 第4篇：MySQL基本查询进阶

#### 创建联结

- SQL最强大的功能之一就是能在数据检索查询的执行中联结（join）表。

- 外键（foreign key） 外键为某个表中的一列，它包含另一个表的主键值，定义了两个表之间的关系。
- **注意：在实际开发中，一般只会使用逻辑外键，而非真正的物理外键。**
- 为什么要使用联结：将本来在一张表中的多个数据进行拆分，但是原本在一张表中的数据，被拆分为两张或者多张表，如何查询数据？那就要使用到联结。使用特殊的语法，可以联结多个表返回一组输出，联结在运行时关联表中正确的行。
- 创建联结

```mysql
SELECT vend_name,prod_name,prod_price FROM vendors,products WHERE vendors.vend_id = products.vend_id ORDER BY vend_name,prod_name;
```

![1608510434800](images/1608510434800.png)

- **完全限定列名** 在引用的列可能出现二义性时，必须使用完全限定列名（用一个点分隔的表名和列名）。如果引用一个没有用表名限制的具有二义性的列名，MySQL将返回错误。
- 笛卡儿积（cartesian product） 由没有联结条件的表关系返回的结果为笛卡儿积。检索出的行的数目将是第一个表中的行数乘以第二个表中的行数。

```mysql
SELECT vend_name,prod_name,prod_price FROM vendors,products ORDER BY vend_name,prod_name;
```

![1608510585670](images/1608510585670.png)

- 不要忘了WHERE子句 应该保证所有联结都有WHERE子句，否则MySQL将返回比想要的数据多得多的数据。同理，应该保证WHERE子句的正确性。不正确的过滤条件将导致MySQL返回不正确的数据。
- **内部联结：等值联结**

```mysql
SELECT vend_name,prod_name,prod_price FROM vendors INNER JOIN products ON vendors.vend_id = products.vend_id ORDER BY vend_name,prod_name;
```

![1608510740571](images/1608510740571.png)

- 联结多个表

```mysql
SELECT vend_name,prod_name,prod_price,quantity FROM vendors,products,orderitems WHERE vendors.vend_id = products.vend_id AND orderitems.prod_id = products.prod_id AND order_num = 20005;
```

![1608510881066](images/1608510881066.png)

- 修改之前的案例SQL

```mysql
# 三个联结查询
SELECT cust_name,cust_contact FROM customers WHERE cust_id IN (SELECT cust_id FROM orders WHERE order_num IN (SELECT order_num FROM orderitems WHERE prod_id = 'TNT2'));
# 修改之后
SELECT cust_name,cust_contact FROM customers,orders,orderitems WHERE customers.cust_id = orders.cust_id AND orderitems.order_num = orders.order_num AND prod_id = 'TNT2';
```

![1608511097812](images/1608511097812.png)

#### 创建高级联结

- 使用表别名，在之前介绍了别名，使用关键字 AS 来替换被引用的列。语法如下

```mysql
SELECT Concat(RTrim(vend_name),'(',RTrim(vend_country),')') AS vend_titie FROM vendors ORDER BY vend_name;
```

- 别名除了可以用于列名和计算字段之外，SQL还允许给表名起别名。语法一样，AS可以省略，好处如下
  - 缩短SQL语句
  - 允许在单条SELECT中多次使用相同的表

```mysql
SELECT cust_name,cust_contact FROM customers AS c,orders AS o,orderitems AS oi WHERE c.cust_id = o.cust_id AND oi.order_num = o.order_num AND prod_id = 'TNT2';
```

![1608511548833](images/1608511548833.png)

- 自联结 
- 使用表别名的主要原因之一是能在单条SELECT语句中不止一次引用相同的表。下面举一个例子。
- 假如你发现某物品（其ID为DTNTR）存在问题，因此想知道生产该物品的供应商生产的其他物品是否也存在这些问题。此查询要求首先找到生产ID为DTNTR的物品的供应商，然后找出这个供应商生产的其他物品。下面是解决此问题的一种方法

```mysql
SELECT prod_id,prod_name FROM products WHERE vend_id = (SELECT vend_id FROM products WHERE prod_id = 'DTNTR');
```

![1608590089358](images/1608590089358.png)

- 使用联结查询实现

```mysql
SELECT p1.prod_id,p1.prod_name FROM products p1,products p2 WHERE p1.vend_id = p2.vend_id AND  p2.prod_id = 'DTNTR';
```

![1608590287651](images/1608590287651.png)

- 自然联结

```mysql
SELECT c.*,o.order_num,o.order_date,oi.prod_id,oi.quantity,oi.item_price FROM customers AS c,orders AS o,orderitems AS oi WHERE c.cust_id = o.cust_id AND oi.order_num = o.order_num AND prod_id = 'FB';
```

![1608590568945](images/1608590568945.png)

- 外部联结

```mysql
# 内部联结示例
SELECT customers.cust_id,orders.order_num FROM customers INNER JOIN orders ON customers.cust_id = orders.cust_id;
```

![1608590766380](images/1608590766380.png)

```mysql
# 外部联结
# 外部联结示例
SELECT customers.cust_id,orders.order_num FROM customers LEFT OUTER JOIN orders ON customers.cust_id = orders.cust_id;
```

![1608590889005](images/1608590889005.png)

- 注意：在使用OUTER JOIN语法时，必须使用RIGHT或LEFT关键字指定包括其所有行的表（RIGHT指出的是OUTER JOIN右边的表，而LEFT指出的是OUTER JOIN左边的表）。上面的例子使用LEFT OUTER JOIN从FROM子句的左边表（customers表）中选择所有行。

```mysql
SELECT customers.cust_id,orders.order_num FROM customers RIGHT OUTER JOIN orders ON customers.cust_id = orders.cust_id;
# 注意，关键字 OUTER 是可以省略的，如下
SELECT customers.cust_id,orders.order_num FROM customers RIGHT JOIN orders ON customers.cust_id = orders.cust_id;
```

![1608591029189](images/1608591029189.png)

- 外部联结的类型 存在两种基本的外部联结形式：左外部联结和右外部联结。它们之间的唯一差别是所关联的表的顺序不同。换句话说，左外部联结可通过颠倒FROM或WHERE子句中表的顺序转换为右外部联结。因此，两种类型的外部联结可互换使用，而究竟使用哪一种纯粹是根据方便而定。

- **使用带有聚合函数的联结**

```mysql
SELECT customers.cust_name,customers.cust_id,COUNT(orders.order_num) AS num_ord FROM customers INNER JOIN orders ON customers.cust_id = orders.cust_id GROUP BY customers.cust_id;
```

![1608591324281](images/1608591324281.png)

- 使用联结和联结条件
  - 注意使用的联结类型。一般我们使用内部联结，但是使用外部联结也是有效的。
  - 保证使用正确的联结条件，否则将返回不正确的数据。
  - 应该总是提供联结条件，否则会出现笛卡尔积。
  - 在一个联结中可以包含多个表，甚至对于每个联结可以使用不同的联结类型。虽然这样做是合法的，一般也很有用，但是应该在一起测试他们之前，分别测试每个联结。这将使得故障排除更加简单。

#### 组合查询

- MySQL允许执行多个查询（多条SELECT语句），并将结果作为单个查询结果集返回。这些组合查询通常称为并（union）或复合查询（compound query）。
- 有两种基本情况，其中需要使用组合查询
  - 在单个查询中从不同的表返回类似结构的数据；
  - 对单个表执行多个查询，按单个查询返回数据
- 创建组合查询，可以用UNION操作符组合数条SQL查询。

```mysql
# 查询
SELECT vend_id,prod_id,prod_price FROM products WHERE prod_price <=5;
# 多条
SELECT vend_id,prod_id,prod_price FROM products WHERE vend_id IN (1001,1002);
# 组合查询
SELECT vend_id,prod_id,prod_price FROM products WHERE prod_price <=5 UNION SELECT vend_id,prod_id,prod_price FROM products WHERE vend_id IN (1001,1002);
```

![1608592275031](images/1608592275031.png)

- UNION 规则
  - UNION 必须由2条或者2条以上的SELECT语句组成，语句之间使用UNION关键字分隔（因此，如果组合4条SELECT语句，将要使用3个UNION关键字）
  - UNION中的每个查询都必须包含相同的列、表达式、或者聚集函数（不过各个列不需要以相同的次序列出）
  - 列数据类型必须兼容：类型不必完全相同，但必须是DBMS可以隐含地转换的类型（例如，不同的数值类型或不同的日期类型）
- 取消活包含重复的行
  - UNION从查询结果集中自动去除了重复的行
  - 如果UNION ALL ，则可以返回所有的行

```mysql
SELECT vend_id,prod_id,prod_price FROM products WHERE prod_price <=5 UNION ALL SELECT vend_id,prod_id,prod_price FROM products WHERE vend_id IN (1001,1002);
```

![1608592603426](images/1608592603426.png)

- 对组合查询的结果进行排序

```mysql
SELECT vend_id,prod_id,prod_price FROM products WHERE prod_price <=5 UNION ALL SELECT vend_id,prod_id,prod_price FROM products WHERE vend_id IN (1001,1002) ORDER BY vend_id,prod_price;
```

![1608592695471](images/1608592695471.png)

#### 全文本搜索

- 并非所有引擎都支持全文本搜索。两个最常使用的引擎为MyISAM和InnoDB，前者支持全文本搜索，而后者不支持。这就是为什么虽然本书中创建的多数样例表使用InnoDB，而有一个样例表（productnotes表）却使用MyISAM的原因。如果你的应用中需要全文本搜索功能，应该记住这一点。
- 需要对搜索的列建立全文索引
- 在之前的学习中，学习了LIKE搜索和正则表达式搜索，但是，请注意以下几点
  - 性能：通配符和正则表达式匹配通常要求MySQL尝试匹配表中所有行（而且这些搜索极少使用表索引）。因此，由于被搜索行数不断增加，这些搜索可能非常耗时。
  - 明确控制：使用通配符和正则表达式匹配，很难（而且并不总是能）明确地控制匹配什么和不匹配什么。例如，指定一个词必须匹配，一个词必须不匹配，而一个词仅在第一个词确实匹配的情况下才可以匹配或者才可以不匹配。
  - 智能化的结果：虽然基于通配符和正则表达式的搜索提供了非常灵活的搜索，但它们都不能提供一种智能化的选择结果的方法。例如，一个特殊词的搜索将会返回包含该词的所有行，而不区分包含单个匹配的行和包含多个匹配的行（按照可能是更好的匹配来排列它们）。类似，一个特殊词的搜索将不会找出不包含该词但包含其他相关词的行。
- 使用全文搜索索引：在之前的数据表中，productnotes 是 使用 MYISAM 搜索引擎，因为MySQL5.7默认使用INNODB搜索引擎，INNODB不支持全文搜索
- 在索引之后，使用两个函数Match()和Against()执行全文本搜索，其中Match()指定被搜索的列，Against()指定要使用的搜索表达式。

```mysql
SELECT note_text FROM productnotes WHERE Match(note_text) Against('rabbit');
```

![1608593695322](images/1608593695322.png)

- 此SELECT语句检索单个列note_text。由于WHERE子句，一个全文本搜索被执行。Match(note_text)指示MySQL针对指定的列进行搜索，Against('rabbit')指定词rabbit作为搜索文本。由于有两行包含词rabbit，这两个行被返回。
- 使用完整的Match()说明 传递给Match()的值必须与FULLTEXT()定义中的相同。如果指定多个列，则必须列出它们（而且次序正确）
- 搜索不区分大小写 除非使用BINARY方式（本章中没有介绍），否则全文本搜索不区分大小写。
- 两个行都包含词rabbit，但包含词rabbit作为第3个词的行的等级比作为第20个词的行高。这很重要。全文本搜索的一个重要部分就是对结果排序。具有较高等级的行先返回（因为这些行很可能是你真正想要的行）。

```mysql
SELECT note_text,Match(note_text) Against('rabbit') AS rank FROM productnotes;
```

![1608593922568](images/1608593922568.png)

- 这里，在SELECT而不是WHERE子句中使用Match()和Against()。这使所有行都被返回（因为没有WHERE子句）。Match()和Against()用来建立一个计算列（别名为rank），此列包含全文本搜索计算出的等级值。等级由MySQL根据行中词的数目、唯一词的数目、整个索引中词的总数以及包含该词的行的数目计算出来。正如所见，不包含词rabbit的行等级为0（因此不被前一例子中的WHERE子句选择）。确实包含词rabbit的两个行每行都有一个等级值，文本中词靠前的行的等级值比词靠后的行的等级值高。
- **使用查询拓展** ：在使用查询扩展时，MySQL对数据和索引进行两遍扫描来完成搜索：
  - 首先，进行一个基本的全文本搜索，找出与搜索条件匹配的所有行；
  - 其次，MySQL检查这些匹配行并选择所有有用的词（我们将会简要地解释MySQL如何断定什么有用，什么无用）
  - 再其次，MySQL再次进行全文本搜索，这次不仅使用原来的条件，而且还使用所有有用的词。

```mysql
SELECT note_text FROM productnotes WHERE Match(note_text) Against('anvils');
```

![1608594161573](images/1608594161573.png)

- **使用查询扩展**

```mysql
SELECT note_text FROM productnotes WHERE Match(note_text) Against('anvils' WITH QUERY EXPANSION);
```

![1608594290905](images/1608594290905.png)

- **布尔文本搜索：**  以布尔方式，可以提供关于如下内容的细节
  - 要匹配的词；
  - 要排斥的词（如果某行包含这个词，则不返回该行，即使它包含其他指定的词也是如此）
  - 排列提示（指定某些词比其他词更重要，更重要的词等级更高）；
  - 表达式分组；
  - 另外一些内容。
- 即使没有FULLTEXT索引也可以使用 布尔方式不同于迄今为止使用的全文本搜索语法的地方在于，即使没有定义FULLTEXT索引，也可以使用它。但这是一种非常缓慢的操作（其性能将随着数据量的增加而降低）
- 为演示IN BOOLEAN MODE的作用，举一个简单的例子

```mysql
SELECT note_text FROM productnotes WHERE Match(note_text) Against('heavy' IN BOOLEAN MODE);
```

![1608594622734](images/1608594622734.png)

- 为了匹配包含heavy但不包含任意以rope开始的词的行，可使用以下查询

```mysql
SELECT note_text FROM productnotes WHERE Match(note_text) Against('heavy -rope*' IN BOOLEAN MODE);
```

![1608594717146](images/1608594717146.png)

- 这次只返回一行。这一次仍然匹配词heavy，但-rope\*明确地指示MySQL排除包含rope*（任何以rope开始的词，包括ropes）的行，这就是为什么上一个例子中的第一行被排除的原因

| 布尔操作符 | 说明                                                         |
| ---------- | ------------------------------------------------------------ |
| +          | 包含，词必须存在                                             |
| -          | 排除，词必须不存在                                           |
| >          | 包含，而且增加等级值                                         |
| <          | 排除，而且减少等级值                                         |
| ()         | 把词组成子表达式（允许这些子表达式作为一个组被包含、排除、排列等） |
| ~          | 取消一个词的排序值                                           |
| *          | 词尾的通配符                                                 |
| ""         | 定义一个短语（与单个词的列表不一样，它匹配整个短语以便包含或排除这个短语） |

- **全文本搜索的使用说明**
  - 在索引全文本数据时，短词被忽略且从索引中排除。短词定义为那些具有3个或3个以下字符的词（如果需要，这个数目可以更改）。
  - MySQL带有一个内建的非用词（stopword）列表，这些词在索引全文本数据时总是被忽略。如果需要，可以覆盖这个列表（请参阅MySQL文档以了解如何完成此工作）。
  - 许多词出现的频率很高，搜索它们没有用处（返回太多的结果）。因此，MySQL规定了一条50%规则，如果一个词出现在50%以上的行中，则将它作为一个非用词忽略。50%规则不用于IN BOOLEAN MODE。
  - 如果表中的行数少于3行，则全文本搜索不返回结果（因为每个词或者不出现，或者至少出现在50%的行中）。
  - 忽略词中的单引号。例如，don't索引为dont。
  - 不具有词分隔符（包括日语和汉语）的语言不能恰当地返回全文本搜索结果。
  - 如前所述，仅在MyISAM数据库引擎中支持全文本搜索。 

#### 参考

- 《MySQL必知必会》

